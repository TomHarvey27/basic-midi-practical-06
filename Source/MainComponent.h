/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public MidiInputCallback, public Button::Listener,public MidiMessage, public Slider::Listener, public ComboBox::Listener

{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void handleIncomingMidiMessage (MidiInput* midiInput, const MidiMessage& message) override;
    void buttonClicked (Button* button) override;

    void sliderValueChanged (Slider* slider) override;
    
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
private:
    
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    
    ComboBox messageTypeBox;
    ComboBox channelBox;
    Slider numberBox;
    Slider velocityBox;
    TextButton sendButton;
    
    Label MessageTypeLabel;
    
    
    uint8 velocity;
    int noteOn;
    int channel;
    
    MidiOutput* midiOut;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
