/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent() : velocity(0)
{
    setSize (500, 400);
    
    StringArray names = MidiInput::getDevices();
    
    for (int i = 0; i < names.size(); i++)
    {
        std::cout << names[i] << std::endl;
    }
    
    midiOut = MidiOutput::createNewDevice("MyMidiDevice"); // Makes a new Midi output
    
    audioDeviceManager.setMidiInputEnabled ("LPK25", true);     // Sets the input from a keyboard
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
   audioDeviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");  //Sets the default output to the synth
    
    midiLabel.setText("midiLabel", sendNotification);
    addAndMakeVisible(midiLabel);
    
    
    
    messageTypeBox.addItem("Note", 01);
    messageTypeBox.addItem("Pitch bend", 02);
    addAndMakeVisible(messageTypeBox);
    
    channelBox.addItem("1", 01);
    channelBox.addItem("2", 02);
    channelBox.addItem("3", 03);
    channelBox.addItem("4", 04);
    addAndMakeVisible(channelBox);

    numberBox.setSliderStyle(numberBox.IncDecButtons);
    numberBox.setRange(0, 127, 1);
    numberBox.addListener(this);
    addAndMakeVisible(numberBox);
    
    velocityBox.setSliderStyle(velocityBox.IncDecButtons);
    velocityBox.setRange(0, 127, 1);
    velocityBox.addListener(this);
    addAndMakeVisible(velocityBox);
    
    sendButton.setButtonText("Send");
    addAndMakeVisible(sendButton);
    sendButton.addListener(this);
    
    MessageTypeLabel.setText("Message Type", sendNotification);
    addAndMakeVisible(MessageTypeLabel);
    MessageTypeLabel.attachToComponent(&messageTypeBox, true);

    MidiMessage message;
    

}

MainComponent::~MainComponent()
{

    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    delete midiOut;
}

void MainComponent::resized()
{
  
    
    messageTypeBox.setBounds (0 * (getWidth()/5), 30, getWidth()/5, 30);
    channelBox.setBounds (1 * (getWidth()/5), 30, getWidth()/5, 30);
    numberBox.setBounds (2 * (getWidth()/5), 30, getWidth()/5, 30);
    velocityBox.setBounds (3 * (getWidth()/5), 30, getWidth()/5, 30);
    sendButton.setBounds (4 * (getWidth()/5), 30, getWidth()/5, 30);
}

void MainComponent::handleIncomingMidiMessage(MidiInput* midiInput, const MidiMessage& message)
{
    String midiText;
    
   audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
   
    if (message.isNoteOnOrOff()) {
        midiText << "NoteOn: Channel " << message.getChannel(); midiText << ":Number" << message.getNoteNumber(); midiText << ":Velocity" << message.getVelocity();
    }
    

    midiLabel.getTextValue() = midiText;
    
    // this function basically prints the input of the virtual synth to text in the window
    
}

void MainComponent::buttonClicked (Button* button)
{
    String midiDataMessage;
    int channel = 1;// channelBox.getSelectedId();
    //int velocity = 127;
    
    if (button == &sendButton)
    {
        DBG("MessageSent");
        
        //midiOut->sendMessageNow(MidiMessage::noteOn(channel, noteOn, velocity));
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(MidiMessage::noteOn(channel, noteOn, velocity));
    }

}
void MainComponent::sliderValueChanged(Slider* slider) // This slider is a pointer
{
    if (slider == &velocityBox) // If the pointer is pointing to velocity box
    {
        velocity = slider->getValue(); //Change the value to the value of the slider
    }
    
    else if (slider == &numberBox)
    {
       noteOn = slider->getValue();
    }
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    
    if (comboBoxThatHasChanged == &channelBox)
    {
        channel = comboBoxThatHasChanged->getSelectedId();
    }
    
}